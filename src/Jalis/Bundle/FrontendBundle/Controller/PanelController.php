<?php

namespace Jalis\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class PanelController extends Controller
{
    /**
     * @Route("/panel",name="panel_home")
     * @Template()
     */
    public function indexAction()
    {
        $this->em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $aquariums = $this->em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user,array());

        $sql = "SELECT count(id) AS numero
                        FROM Tenant
                        WHERE user_id = " . $user->getId();

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        $tenants = $stmt->fetchColumn();


        if ($aquariums) {

        $sql2 = "SELECT count(id) AS numero
                        FROM water_parameter
                        WHERE aquarium_id = " . $aquariums[0]->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $parameters = $stmt->fetchColumn();

        $sql2 =  "SELECT count(id) AS numero
                        FROM Device
                        WHERE aquarium_id = " . $aquariums[0]->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $devices = $stmt->fetchColumn();

         }

        $sql2 =  "SELECT count(id) AS numero
                        FROM event
                        WHERE user_id = ".$user->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $alerts = $stmt->fetchColumn();


        $sql2 =  "SELECT DISTINCT(b.id),b.*
                        FROM BadgeUser u, Badge b
                        WHERE  b.id = u.badge_id
                        AND u.user_id = ".$user->getId();

        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $badges = $stmt->fetchAll();



        $this->generateQr($user);

        return array(
            'user' => $user,
            'name' => 'portada',
            'parameters' => $parameters,
            'alerts' => $alerts,
            'devices' => $devices,
            'badges' => $badges,
            'tenants' => $tenants

        );

    }

    /**
     * @Route("/all-users/{page}",defaults={"page" = 1},name="all-users")
     * @Template()
     */
    public function allUsersAction($page,Request $request)
    {

        $this->em = $this->get('doctrine')->getManager();


        $pageSize = 12;
        $repository = $this->em->getRepository('JalisUserBundle:User');


        $filter = $request->request->get('filter');

        $qb = $repository->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        if($filter) {
            $qb = $qb->Where("a.nick like '%".$filter."%'");

        }


        $total = $qb->getQuery()->getSingleScalarResult();

        $query = $repository->createQueryBuilder('p');

        if($filter) {
            $query = $query->Where("p.nick like '%".$filter."%'");

        }

        $query = $query->setFirstResult($pageSize * ($page - 1))
            ->setMaxResults($pageSize)
            ->getQuery();


        $users = $query->getResult();

        foreach($users as $userobject) {

            $user['id'] = $userobject->getId();


            $data[$user['id']]['username'] = $userobject->getUsername();
            $data[$user['id']]['devices'] = 0;

            $sql2 = "SELECT count(id) FROM Tenant t where t.user_id = ".$user['id'];
            $stmt = $this->em->getConnection()->prepare($sql2);

            $stmt->execute();
            $tenants = $stmt->fetchColumn();
            $data[$user['id']]['tenants'] = $tenants;

            $sql2 = "SELECT * FROM aquarium t where t.user_id = ".$user['id'];
            $stmt = $this->em->getConnection()->prepare($sql2);

            $stmt->execute();
            $acuarios = $stmt->fetchAll();
            $data[$user['id']]['aquarium'] = $acuarios[0]['id'];

            $data[$user['id']]['events'] = 0;
            if($acuarios[0]['id'] != null) {
                $sql2 = "SELECT count(id) FROM water_parameter t where t.aquarium_id = " . $acuarios[0]['id'];
                $stmt = $this->em->getConnection()->prepare($sql2);

                $stmt->execute();
                $mediciones = $stmt->fetchColumn();
                $data[$user['id']]['waterparameters'] = $mediciones;

                $sql3 = "SELECT count(id) FROM Device t where t.aquarium_id = " . $acuarios[0]['id'];
                $stmt = $this->em->getConnection()->prepare($sql3);

                $stmt->execute();
                $devices = $stmt->fetchColumn();
                $data[$user['id']]['devices'] = $devices;


                $photo = $this->em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $acuarios[0]['id'] ,'obj_type' =>'Aquarium'),array('id' =>'DESC'));
                $eventos = $this->em->getRepository('CrijaReminderBundle:Event')->findOneBy(array('user' => $user) , array('id' =>'DESC'));

                $sql2 =  "SELECT DISTINCT(b.id),b.*
                        FROM BadgeUser u, Badge b
                        WHERE  b.id = u.badge_id
                        AND u.user_id = ".$user['id'];

                $stmt = $this->em->getConnection()->prepare($sql2);
                $stmt->execute();
                $badges = $stmt->fetchAll();

                if($badges != null ) {
                    $data[$user['id']]['badges'] = $badges;
                } else {
                    $data[$user['id']]['badges'] = 0;
                }


                if($eventos != null ) {
                    $data[$user['id']]['events'] = count($eventos);
                } else {
                    $data[$user['id']]['events'] = 0;
                }
                $data[$user['id']]['events'] = 0;
                $data[$user['id']]['events'] = count($eventos);
                if($photo != null) {

                    $data[$user['id']]['photo'] = $photo->getWebpath();
                } else {
                    $data[$user['id']]['photo'] = "/frontendbundle/images/pic0".rand(1,4).".jpg";
                }

            } else {
                $data[$user['id']]['waterparameters'] = 0;
                $data[$user['id']]['photo'] = "/frontendbundle/images/pic0".rand(1,4).".jpg";
            }




        }
        $total_pages = ceil($total/$pageSize);
        return array(
            'entities' => $data,
             'total_pages' => $total_pages,
        'total' => $total,
         'page' => $page,
        );

    }

    /**
     * @Route("/test",name="panel_test")
     * @Template()
     */
    public function testAction()
    {

        $user = $this->get('security.context')->getToken()->getUser();



        return array(
            'user' => $user,
            'name' => 'portada'
        );

    }
    /**
     * generate qr
     */
    public function generateQr($user)
    {
        $fs = new Filesystem();

        $dataToQr = "https://www.mycoralreef.es/usuario/".$user->getUsername();


        $qr_path = "/var/www/vhosts/mycoralreef.es/httpdocs/web/qr/";

        try {
            $fs->mkdir($qr_path);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while creating your directory at ".$e->getPath();
        }
        if (!file_exists($qr_path.$user->getId().".png")) {

         $generated = null;

            if($dataToQr != null) {

                $process = new Process("qrencode -o ".$qr_path.$user->getId().".png '".$dataToQr."'");
                $process->run();

                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    throw new \RuntimeException($process->getErrorOutput());
                }


            }

        }

    }
    /**
     * @Route("/tools",name="panel_tools")
     * @Template()
     */
    public function toolsAction()
    {
        $this->em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();



        return array(
            'user' => $user

        );

    }
    /**
     * @Route("/generate_ws_token",name="generate_ws_token")
     * @Template()
     */
    public function generateTokenAction()
    {
        $em = $this->get('doctrine')->getManager();
        $user = $this->get('security.context')->getToken()->getUser();


        $token= md5(uniqid(rand(), true));
        
        $user->setWsToken($token);
        $em->persist($user);
        $em->flush();


        return $this->redirect($this->generateUrl('panel_tools'));

    }

    /**
     * @Route("/utiles",name="utiles")
     * @Template()
     */
    public function utilesAction()
    {

        return array(
          'ok'=> 'ok'
        );

    }


}
