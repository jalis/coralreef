<?php
namespace Jalis\Bundle\FrontendBundle\Controller;

use Crija\Bundle\AquariumBundle\Entity\WaterParameter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Jalis\Bundle\UserBundle\Entity\BadgeUser;
use Crija\Bundle\ReminderBundle\Entity\Notify;


class WsController extends Controller
{
    /**
     * @Route("/ws/new_parameter",name="ws_new_parameter")
     * @Template()
     */
    public function generateTokenAction(Request $request)
    {


        $em = $this->get('doctrine')->getManager();
        $token = $request->query->get('token');
        $ph = $request->query->get('ph');
        $temperature = $request->query->get('temperature');

        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        if(!$user) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'token no valido');
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }

        $aquarium = $em->getRepository('CrijaAquariumBundle:Aquarium')->findOneBy(array('user' => $user));
        $last_parameter = $em->getRepository('CrijaAquariumBundle:WaterParameter')->findOneBy(array('aquarium' => $aquarium, 'description' => 'Medicion automatica'), array('id' => 'DESC'));

        $error = null;

        $date1 = new \DateTime();
        if ($last_parameter) {
            $date2 = $last_parameter->getCreatedAt();
        } else {
            $date2 = new \DateTime('2013-01-29');
        }

        $diff = $date2->diff($date1);



        $horas = $diff->format('%h');
        $dia =  $diff->format('%a');

        if($horas < 1 && $dia == 0) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'Solo se permite una medicion automatica cada hora, ultima medicion hace '.$horas.' horas y '.$dia.' dias');
        }

        $sql2 = "SELECT * FROM aquarium q,water_parameter w where q.id =".$aquarium->getId()." and q.user_id =".$user->getId()." and w.aquarium_id = ".$aquarium->getId()." AND (w.description like 'Medicion automatica') order By w.created_at asc";
        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $medicion_automatica = $stmt->fetchAll();



        if($error == null) {

            $newParameter = new WaterParameter();

            $newParameter->setPh($ph);
            $newParameter->setTemperature($temperature);
            $newParameter->setDescription('Medicion automatica');
            $newParameter->setAquarium($aquarium);
            $newParameter->setCreatedAt($date1);
            $newParameter->setUpdatedAt($date1);


            $em->persist($newParameter);




            if(count($medicion_automatica) == 0) {
                $this->newBadge(3,$user->getId());
            }

            $em->flush();


            $data = array('token'=> $token,'cod' => 0 , 'msg' => 'Sin errores, medicion automatica anterior hace '.$horas.' horas y '.$dia.' dias');
            $response = new JsonResponse($data, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        } else {
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }


    }


    /**
     * @Route("/ws/new_app_parameter",name="ws_new_app_parameter")
     * @Template()
     */
    public function newApParameterAction(Request $request)
    {

        $date1 = new \DateTime();
        $em = $this->get('doctrine')->getManager();
        $token = $request->query->get('token');

        $temperature = $request->query->get('temperature');
        $salinity       = $request->query->get('salinity');
        $calcium        = $request->query->get('calcium');
        $ph             = $request->query->get('ph');
        $magnesium      = $request->query->get('magnesium');
        $phosphate      = $request->query->get('phosphate');
        $alkalinity     = $request->query->get('alkalinity');
        $ammonia        = $request->query->get('ammonia');
        $silica         = $request->query->get('silica');
        $iodine         = $request->query->get('iodine');
        $nitrate        = $request->query->get('nitrate');
        $nitrite        = $request->query->get('nitrite');
        $boron          = $request->query->get('boron');
        $iron           = $request->query->get('iron');
        $strontium      = $request->query->get('strontium');
        $potassium      = $request->query->get('potassium');
        $description    = $request->query->get('description');

        $error = null;

        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        if(!$user) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'token no valido');
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }

        $aquarium = $em->getRepository('CrijaAquariumBundle:Aquarium')->findOneBy(array('user' => $user));


        if($error == null) {

            $newParameter = new WaterParameter();

            $newParameter->setPh($ph);
            $newParameter->setTemperature($temperature);
            $newParameter->setSalinity($salinity);
            $newParameter->setCalcium($calcium);
            $newParameter->setMagnesium($magnesium);
            $newParameter->setPhosphate($phosphate);
            $newParameter->setAlkalinity($alkalinity);
            $newParameter->setAmmonia($ammonia);
            $newParameter->setSilica($silica);
            $newParameter->setIodine($iodine);
            $newParameter->setNitrate($nitrate);
            $newParameter->setNitrite($nitrite);
            $newParameter->setBoron($boron);
            $newParameter->setIron($iron);
            $newParameter->setStrontium($strontium);
            $newParameter->setPotassium($potassium);


            $newParameter->setDescription($description);
            $newParameter->setAquarium($aquarium);
            $newParameter->setCreatedAt($date1);
            $newParameter->setUpdatedAt($date1);


            $em->persist($newParameter);
            $em->flush();


            $data = array('token'=> $token,'cod' => 0 , 'msg' => 'Sin errores');
            $response = new JsonResponse($data, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        } else {
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }


    }


    /**
     * @Route("/ws/login",name="ws_login")
     * @Template()
     */
    public function loginAction(Request $request)
    {

        $em = $this->get('doctrine')->getManager();
        $username = $request->query->get('username');
        $password = $request->query->get('password');



        $user_manager = $this->get('fos_user.user_provider.username');
        $factory = $this->get('security.encoder_factory');

        $user = $user_manager->loadUserByUsername($username);

        $encoder = $factory->getEncoder($user);

        $bool = ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) ? true : false;

    
        if($bool === true) {
            $data = array('token'=> $user->getWsToken(),'username'=> $user->getUsername(),'cod' => 0 , 'msg' => 'login OK');
        } else if($bool === false) {
            $data = array('cod' => 1 , 'msg' => 'login KO');
        }


        $response = new JsonResponse($data, 200, array());
        $callback = 'callback';
        $response->setCallback($callback);
        return $response;


    }

    /**
     * get last parameters.
     * @Route("/ws/last_parameter",name="ws_last_parameter")
     * @Template()
     */
    public function lastParametersAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $token = $request->query->get('token');


        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        if(!$user) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'token no valido');
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }

        $keys = array('salinity','calcium','ph','temperature','magnesium','phosphate','alkalinity','ammonia','silica','iodine','nitrate','nitrite','boron','iron','strontium','potassium');

        if(!$user) { die("error no user"); }

        $aquariums = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        $ultima_medicion = array();

        foreach($aquariums as $aquarium) {

            $has_parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->findBy(array("aquarium" => $aquarium));
            if(count($has_parameters) > 0)
            {
                foreach($keys as $key) {
                    $parameters = $em->getRepository('CrijaAquariumBundle:WaterParameter')->getLastValuesByKey($aquarium,$key,2);

                    $parameter_last[$key] = $parameters[0];
                    if($parameters[1] != null ) {
                        $parameter_before[$key] = $parameters[1];
                    }
                    else
                    {
                        $parameter_before[$key] = $parameters[0];
                    }
                }
                $aquariums_array[] = array("last" => $parameter_last,"before" => $parameter_before,"name" => $aquarium->getName());

            }
            else {
                $aquariums_array[] = array("last" => null,"before" => null,"name" => $aquarium->getName());
            }
        }



        $response = new JsonResponse($aquariums_array, 200, array());
        $callback = 'callback';
        $response->setCallback($callback);
        return $response;

    }

    /**
     * Lists all Aquarium entities.
     *
     * @Route("/ws/aquariums", name="ws_aquariums")
     * @Template()
     */
    public function aquariumsAction(Request $request)
    {
        $em = $this->get('doctrine')->getManager();
        $token = $request->query->get('token');


        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        if(!$user) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'token no valido');
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }

        $entities = $em->getRepository('CrijaAquariumBundle:Aquarium')->findByUser($user);

        $gallery= array();
        foreach($entities as $entity)
        {

            $data = $em->getRepository('JalisGalleryBundle:Photo')->findOneBy(array('obj_id' => $entity->getId(),'obj_type' =>'Aquarium'),array('id' =>'DESC'));
            $aquariums[]['id'] = $entity->getId();
            $aquariums[]['name'] = $entity->getName();
            $aquariums[]['description'] = $entity->getDescription();
            if($data != null) {
                $gallery[$entity->getId()]= $data->getWebpath();
            } else {
                $gallery[$entity->getId()]= "/frontendbundle/images/pic0".rand(1,4).".jpg";
            }

        }

        $datos = array('entities' => $aquariums, 'gallery' => $gallery);

        $response = new JsonResponse($datos, 200, array());
        $callback = 'callback';
        $response->setCallback($callback);
        return $response;

    }

    /**
     * newbadge.
     */
    private function newBadge($badgeId,$userId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => 1));
        $toUser = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => $userId));
        $badge =  $entity = $em->getRepository('JalisUserBundle:Badge')->find($badgeId);

        $badgeUser = new BadgeUser();
        $badgeUser->setBadges($badge);
        $badgeUser->setUser($toUser);
        $em->persist($badgeUser);



        $notify = new Notify();
        $badgeimage = "<br><br><img src=\'/badges/".$badge->getImage()."\'><br><br>";
        $notify->setMessage("<center>".$badgeimage." ".$badge->getDescription()."</center>");
        $notify->setUserTo($toUser->getUsername());
        $notify->setTitle($badge->getTitle());
        $notify->setUserFrom($user->getUsername());
        $notify->setType("premio");
        $em->persist($notify);

        $em->flush();
    }

    /**
     * widget
     *
     * @Route("/ws/tenants", name="ws_app_tenant")
     * @Template()
     */
    public function appTenantAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $token = $request->query->get('token');


        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('wsToken' => $token));
        if(!$user) {
            $error = array('token'=> $token,'cod' => 1 , 'msg' => 'token no valido');
            $response = new JsonResponse($error, 200, array());
            $callback = 'callback';
            $response->setCallback($callback);
            return $response;
        }

        $entities = $em->getRepository('CrijaAnimalBundle:Tenant')->findBy(array('user' =>$user ),array('diedAt' => 'ASC'));

        foreach($entities as $entity) {



            if($entity->getAnimal()->getAnimalCategory()->getId() == 1)
            {
                $peces[] = (array) $entity;

                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));


            }else if($entity->getAnimal()->getAnimalCategory()->getId() == 2) {
                $corales[] = (array) $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }
            else if($entity->getAnimal()->getAnimalCategory()->getId() == 3) {
                $invertebrados[] = (array) $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }

        }

        $tenants['Peces'] = $peces;
        $tenants['Corales'] = $corales;
        $tenants['Invertebrados'] = $invertebrados;

ld($tenants);
        $datos = array('all_tenants' => $tenants, 'gallery' => $gallery);

        $response = new JsonResponse($datos, 200, array());
        $callback = 'callback';
        $response->setCallback($callback);
        return $response;



    }
}
