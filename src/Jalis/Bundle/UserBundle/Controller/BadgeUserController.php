<?php

namespace Jalis\Bundle\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Jalis\Bundle\UserBundle\Entity\BadgeUser;
use Jalis\Bundle\UserBundle\Form\BadgeUserType;
use Crija\Bundle\ReminderBundle\Entity\Notify;

/**
 * BadgeUser controller.
 *
 * @Route("/badgeuser")
 */
class BadgeUserController extends Controller
{

    /**
     * Lists all BadgeUser entities.
     *
     * @Route("/", name="badgeuser")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('JalisUserBundle:BadgeUser')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all BadgeUser entities.
     *
     * @Route("/mis-logros", name="mis_logros")
     * @Method("GET")
     * @Template()
     */
    public function myBadgesPrivateAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        if(!$user) { die("error no user"); }

         //$entities = $em->getRepository('JalisUserBundle:BadgeUser')->findBy(array('user' => $user));

        $sql2 =  "SELECT DISTINCT(b.id),b.*
                        FROM BadgeUser u, Badge b
                        WHERE  b.id = u.badge_id
                        AND u.user_id = ".$user->getId();

        $stmt = $em->getConnection()->prepare($sql2);
        $stmt->execute();
        $entities = $stmt->fetchAll();



        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new BadgeUser entity.
     *
     * @Route("/", name="badgeuser_create")
     * @Method("POST")
     * @Template("JalisUserBundle:BadgeUser:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new BadgeUser();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);


            $userFrom = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => 1));

            $notify = new Notify();
            $badge = "<br><br><img src=\'/badges/".$entity->getBadges()->getImage()."\'><br><br>";
            $notify->setMessage("<center>".$badge." ".$entity->getBadges()->getDescription()."</center>");
            $notify->setUserTo($entity->getUser()->getUsername());
            $notify->setTitle($entity->getBadges()->getTitle());
            $notify->setUserFrom($userFrom->getUsername());
            $notify->setType("premio");

            $em->persist($notify);
            $em->flush();



            return $this->redirect($this->generateUrl('badgeuser_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a BadgeUser entity.
     *
     * @param BadgeUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BadgeUser $entity)
    {
        $form = $this->createForm(new BadgeUserType(), $entity, array(
            'action' => $this->generateUrl('badgeuser_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BadgeUser entity.
     *
     * @Route("/new", name="badgeuser_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new BadgeUser();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a BadgeUser entity.
     *
     * @Route("/{id}", name="badgeuser_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JalisUserBundle:BadgeUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BadgeUser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing BadgeUser entity.
     *
     * @Route("/{id}/edit", name="badgeuser_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JalisUserBundle:BadgeUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BadgeUser entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a BadgeUser entity.
    *
    * @param BadgeUser $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BadgeUser $entity)
    {
        $form = $this->createForm(new BadgeUserType(), $entity, array(
            'action' => $this->generateUrl('badgeuser_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BadgeUser entity.
     *
     * @Route("/{id}", name="badgeuser_update")
     * @Method("PUT")
     * @Template("JalisUserBundle:BadgeUser:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('JalisUserBundle:BadgeUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BadgeUser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('badgeuser_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a BadgeUser entity.
     *
     * @Route("/{id}", name="badgeuser_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('JalisUserBundle:BadgeUser')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BadgeUser entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('badgeuser'));
    }

    /**
     * Creates a form to delete a BadgeUser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('badgeuser_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
