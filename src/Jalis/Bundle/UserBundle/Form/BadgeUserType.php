<?php

namespace Jalis\Bundle\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BadgeUserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('info')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('user')
            ->add('badges')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jalis\Bundle\UserBundle\Entity\BadgeUser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'jalis_bundle_userbundle_badgeuser';
    }
}
