<?php

namespace Jalis\Bundle\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        
        $builder->add('firstName', 'text', array('label' => 'Nombre'));
        $builder->add('lastName', 'text', array('label' => 'Apellidos'));

        $builder->add('address', 'text', array('label' => 'Direccion'));
        $builder->add('cp', 'text', array('label' => 'Codigo Postal'));
  
    }

    public function getName()
    {
        return 'jalis_user_profile';
    }
}


