<?php

namespace Crija\Bundle\ReminderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="Crija\Bundle\ReminderBundle\Entity\EventRepository")
 */
class Event {

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

 /**
  * @Orm\ManyToOne(targetEntity="Jalis\Bundle\UserBundle\Entity\User", inversedBy="events")
  */
  private $user;

  /**
   * @var string
   *
   * @ORM\Column(name="type", type="string", length=255)
   */
  private $type;

  /**
   * @var boolean
   *
   * @ORM\Column(name="active", type="boolean")
   */
  private $active;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="startedAt", type="datetime")
   */
  private $startedAt;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="createdAt", type="datetime", nullable=true)
   */
  private $createdAt;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
   */
  private $updatedAt;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="completedAt", type="datetime", nullable=true)
   */
  private $completedAt;

  /**
   * @var string
   *
   * @ORM\Column(name="message", type="text", nullable=true)
   */
  private $message;

  /**
   * @var string
   *
   * @ORM\Column(name="periodicity", type="text", nullable=true)
   */
  private $periodicity;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set startedAt
   *
   * @param \DateTime $startedAt
   * @return Event
   */
  public function setStartedAt($startedAt) {
    $this->startedAt = $startedAt;

    return $this;
  }

  /**
   * Get startedAt
   *
   * @return \DateTime 
   */
  public function getStartedAt() {
    return $this->startedAt;
  }

  /**
   * Set user
   *
   * @param string $user
   * @return Event
   */
  public function setUser(\Jalis\Bundle\UserBundle\Entity\User $user = null) {
    $this->user = $user;

    return $this;
  }

  /**
   * Get user
   *
   * @return string 
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * Set type
   *
   * @param string $type
   * @return Event
   */
  public function setType($type) {
    $this->type = $type;

    return $this;
  }

  /**
   * Get type
   *
   * @return string 
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Set active
   *
   * @param boolean $active
   * @return Event
   */
  public function setActive($active) {
    $this->active = $active;

    return $this;
  }

  /**
   * Get active
   *
   * @return boolean 
   */
  public function getActive() {
    return $this->active;
  }

  /**
   * Set createdAt
   *
   * @param \DateTime $createdAt
   * @return Event
   */
  public function setCreatedAt($createdAt) {
    $this->createdAt = $createdAt;

    return $this;
  }

  /**
   * Get createdAt
   *
   * @return \DateTime 
   */
  public function getCreatedAt() {
    return $this->createdAt;
  }

  /**
   * Set updatedAt
   *
   * @param \DateTime $updatedAt
   * @return Event
   */
  public function setUpdatedAt($updatedAt) {
    $this->updatedAt = $updatedAt;

    return $this;
  }

  /**
   * Get updatedAt
   *
   * @return \DateTime 
   */
  public function getUpdatedAt() {
    return $this->updatedAt;
  }

  /**
   * Set completedAt
   *
   * @param \DateTime $completedAt
   * @return Event
   */
  public function setCompletedAt($completedAt) {
    $this->completedAt = $completedAt;

    return $this;
  }

  /**
   * Get completedAt
   *
   * @return \DateTime 
   */
  public function getCompletedAt() {
    return $this->completedAt;
  }

  /**
   * Set message
   *
   * @param string $message
   * @return Event
   */
  public function setMessage($message) {
    $this->message = $message;

    return $this;
  }

  /**
   * Get message
   *
   * @return string 
   */
  public function getMessage() {
    return $this->message;
  }
  
  public function getPeriodicity() {
    return $this->periodicity;
  }

  public function setPeriodicity($periodicity) {
    $this->periodicity = $periodicity;
  }

    
  /**
   *
   * @ORM\PrePersist
   * @ORM\PreUpdate
   */
  public function updatedTimestamps() {
    $this->setUpdatedAt(new \DateTime('now'));

    if ($this->getCreatedAt() == null) {
      $this->setCreatedAt(new \DateTime('now'));
    }
  }
  /**
   * Get CountDown
   *
   * @return string 
   */
  public function getCountDown() {
   
    $today    = new \DateTime('now');
     
    $deadline = new \DateTime($this->completedAt->format('Y-m-d'));


   // $diff     = strtotime($deadline->modify($this->periodicity)->format('Y-m-d')) - strtotime($today->format('Y-m-d'));
      $diff     = strtotime($deadline->format('Y-m-d')) - strtotime($today->format('Y-m-d'));
    return $diff/86400;
    
  }
  /**
   * Get Priority
   *
   * @return string 
   */
  public function getPriority() {
    
    $today    = new \DateTime('now');
    $deadline = new \DateTime($this->completedAt->format('Y-m-d'));
    
 
    $diff     =  strtotime($deadline->modify($this->periodicity)->format('Y-m-d')) - strtotime($today->format('Y-m-d'));
	    
    $daysLeft = $diff/86400;
 
    
    $periodicidad =  (strtotime($this->periodicity) - time())/86400;
 
    $priority = round((($daysLeft*100)/$periodicidad)/4);
      /*
       * $phase =$daysLeft/4;

        if($phase = $priority*1)  {$priority_string = 'danger'; }
        if($phase < $priority*1 &&  $phase <= $priority*2)  {$priority_string = 'warning'; }
        if($phase < $priority*2 &&  $phase <= $priority*3)  {$priority_string = 'info'; }
        if($phase < $priority*4)  {$priority_string = 'success'; }
        */
    
 
    if($priority*1 >= 100) {$priority_string = 'success'; }
    if($priority*1 < 100 && $priority*2 > 50) {$priority_string = 'info'; }
    if($priority*2 <= 50 && $priority*4 > 25) {$priority_string = 'warning'; }
    if($priority*4 <= 25)  {$priority_string = 'danger'; }

    return $priority_string;
  }
  /**
   * Get EndDate
   *
   * @return string 
   */
  public function getEndDate() {
   
    
     
    $deadline = new \DateTime($this->completedAt->format('Y-m-d'));

    $endDate    = $deadline->modify($this->periodicity)->format('Y-m-d');
    
    return $endDate ;
    
  }
    /**
     * Get EndDate
     *
     * @return string
     */
    public function getPeriodo() {

        $periodo = $this->getPeriodicity();
        $periodo = str_replace("+", "", $periodo);
        $periodo = trim(str_replace("days", "", $periodo));

        return $periodo;

    }

}
