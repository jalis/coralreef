<?php

namespace Crija\Bundle\ReminderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
// logger
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Jalis\Bundle\UserBundle\Entity\BadgeUser;
use Crija\Bundle\ReminderBundle\Entity\Notify;

class NotifyBadgeCommand extends ContainerAwareCommand
{
    

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "-1");
        // 0-A. Initialize objects for command.
        parent::initialize($input, $output);
        $this->em     = $this->getContainer()->get('doctrine')->getManager();
    }

    protected function configure()
    {
        $this
                ->setName('crija:notify-badge')
                ->setDescription('Daily process for notify badge.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $notificaciones = array();
        $count = 0;
        $em = $this->getContainer()->get('doctrine')->getManager();
       // $users = $em->getRepository('JalisUserBundle:User')->findAll();
        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => 1));

        $today    = new \DateTime('now');

       /* $sql2 =  "SELECT DISTINCT(u.aquarium_id)
                        FROM water_parameter u
                        WHERE  u.temperature <= '23'";*/

        $sql2 =  "Select * from (SELECT d.aquarium_id,count(d.aquarium_id) as cooo
                        FROM Device d
                       GROUP BY d.aquarium_id) m where m.cooo  >= 10";


        $stmt = $this->em->getConnection()->prepare($sql2);
        $stmt->execute();
        $aquariums = $stmt->fetchAll();

        $userFrom = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => 1));



        foreach($aquariums as $aq) {

            $entity = $em->getRepository('CrijaAquariumBundle:Aquarium')->find($aq['aquarium_id']);
            ld($entity->getUser()->getId());
            $this->newBadge(4,$entity->getUser()->getId());

         }




       $output->writeln("Se han notificado estos eventos:");
       $output->writeln("Total: ".$count);
    }
    /**
     * newbadge.
     */
    private function newBadge($badgeId,$userId)
    {
        $em =  $this->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => 1));
        $toUser = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => $userId));
        $badge =  $entity = $em->getRepository('JalisUserBundle:Badge')->find($badgeId);

        $badgeUser = new BadgeUser();
        $badgeUser->setBadges($badge);
        $badgeUser->setUser($toUser);
        $em->persist($badgeUser);



        $notify = new Notify();
        $badgeimage = "<br><br><img src=\'/badges/".$badge->getImage()."\'><br><br>";
        $notify->setMessage("<center>".$badgeimage." ".$badge->getDescription()."</center>");
        $notify->setUserTo($toUser->getUsername());
        $notify->setTitle($badge->getTitle());
        $notify->setUserFrom($user->getUsername());
        $notify->setType("premio");
        $em->persist($notify);

        $em->flush();
    }
    

}

?>
