<?php

namespace Crija\Bundle\ReminderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotifyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type','choice', array(
                'choices'   => array('global' => 'global','privado' => 'privado', 'premio' => 'premio', 'admin' => 'admin'),
                'required'  => true,
                'empty_value' => 'Elige tipo',
                'label'  => 'Tipo'
            ))
            ->add('message')
            ->add('readAt')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('userFrom','entity',array(
                'property' => 'Uid',
                'class' => 'JalisUserBundle:User',
                'required' => true,
            ))
            ->add('userTo','entity',array(
                'property' => 'Uid',
                'class' => 'JalisUserBundle:User',
                'required' => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\ReminderBundle\Entity\Notify'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_reminderbundle_notify';
    }
}
