<?php

namespace Crija\Bundle\ReminderBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\ReminderBundle\Entity\Notify;
use Crija\Bundle\ReminderBundle\Form\NotifyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Notify controller.
 *
 * @Route("/notify")
 */
class NotifyController extends Controller
{

    /**
     * Lists all Notify entities.
     *
     * @Route("/", name="notify")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CrijaReminderBundle:Notify')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Notify entity.
     *
     * @Route("/notifyToAll", name="notifyToAll")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function notifyToAllAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $enviado = "no";
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }

        if($user->getUsername() != 'myCoralReef') {
            throw $this->createNotFoundException('El usuario '.$user->getUsername().' no puede realizar esta acción');
        }
        $users = $em->getRepository('JalisUserBundle:User')->findAll();

        if($request->isMethod('POST')) {
            $message = $this->get('request')->request->get('message');
            $title = $this->get('request')->request->get('title');
            if(!$message) {
                throw $this->createNotFoundException('falta el mensaje');
            }

            $toUserId = $this->get('request')->request->get('toUser');

            if(!$toUserId){
                foreach($users as $foruser) {
                    $toUser = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => $foruser->getId()));
                    $notify = new Notify();
                    $notify->setMessage($message);
                    $notify->setUserTo($toUser);
                    $notify->setTitle($title);
                    $notify->setUserFrom($user);
                    $notify->setType("privado");

                    $em->persist($notify);



                }
                $em->flush();
              $enviado = count($users).' ok';

            } else {
                $toUser = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => $toUserId));
                $notify = new Notify();
                $notify->setMessage($message);
                $notify->setUserTo($toUser);
                $notify->setTitle($title);
                $notify->setUserFrom($user);
                $notify->setType("privado");

                $em->persist($notify);
                $em->flush();
                $enviado = $toUser->getUsername()." ok";

            }

        }



        return array(
            'users'      => $users,
            'enviado' => $enviado
        );

     }

    /**
     * Creates a new Notify entity.
     *
     * @Route("/", name="notify_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("POST")
     * @Template("CrijaReminderBundle:Notify:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Notify();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('notify_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Notify entity.
     *
     * @param Notify $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Notify $entity)
    {
        $form = $this->createForm(new NotifyType(), $entity, array(
            'action' => $this->generateUrl('notify_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Notify entity.
     *
     * @Route("/new", name="notify_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Notify();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Notify entity.
     *
     * @Route("/{id}", name="notify_show")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Notify')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notify entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Notify entity.
     *
     * @Route("/{id}/edit", name="notify_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Notify')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notify entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Notify entity.
    *
    * @param Notify $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Notify $entity)
    {
        $form = $this->createForm(new NotifyType(), $entity, array(
            'action' => $this->generateUrl('notify_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Notify entity.
     *
     * @Route("/{id}", name="notify_update")
     * @Method("PUT")
     * @Template("CrijaReminderBundle:Notify:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Notify')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notify entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('notify_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Notify entity.
     *
     * @Route("/{id}", name="notify_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaReminderBundle:Notify')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Notify entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('notify'));
    }

    /**
     * Creates a form to delete a Notify entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notify_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }


    /**
     * Finds and displays a Notify entity.
     *
     * @Route("/popup/{userTo}", name="notify_popup")
     * @Method("GET")
     * @Template()
     */
    public function popUpAction($userTo)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('username' => $userTo));

        $notification = $em->getRepository('CrijaReminderBundle:Notify')->findBy(array('userTo' => $userTo, 'readAt' => null));


        foreach($notification as $notify) {

            $notify->setReadAt(new \datetime('now'));
            $em->persist($notify);
            $em->flush();
            $notify->setMessage(stripslashes($notify->getMessage()));
        }


        return array(
            'notification'      => $notification,
        );
    }


}
