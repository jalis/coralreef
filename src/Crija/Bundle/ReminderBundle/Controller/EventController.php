<?php

namespace Crija\Bundle\ReminderBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\ReminderBundle\Entity\Event;
use Crija\Bundle\ReminderBundle\Form\EventType;

/**
 * Event controller.
 *
 * @Route("/event")
 */
class EventController extends Controller
{

    /**
     * Lists all Event entities.
     *
     * @Route("/", name="event")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        
         if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
      
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CrijaReminderBundle:Event')->findby(array('user' => $user));

      
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Event entity.
     *
     * @Route("/", name="event_create")
     * @Method("POST")
     * @Template("CrijaReminderBundle:Event:new.html.twig")
     */
    public function createAction(Request $request)
    {
      
        $user = $this->get('security.context')->getToken()->getUser();
        
         if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
        $today = new \DateTime('now');
        $entity = new Event();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setActive('1');
            $entity->setUser($user);
            $entity->setCompletedAt($entity->getStartedAt());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('event'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Event $entity)
    {
      
        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('event_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Event entity.
     *
     * @Route("/new", name="event_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Event();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Event entity.
     *
     * @Route("/{id}", name="event_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Event entity.
     *
     * @Route("/{id}/edit", name="event_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Event entity.
    *
    * @param Event $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Event $entity)
    {
        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Event entity.
     *
     * @Route("/{id}", name="event_update")
     * @Method("PUT")
     * @Template("CrijaReminderBundle:Event:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaReminderBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('event', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Event entity.
     *
     * @Route("/{id}", name="event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaReminderBundle:Event')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Event entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('event'));
    }

    /**
     * Creates a form to delete a Event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Events widget entity.
     *
     * @Route("/events-widget", name="widget")
     * @Template()
     */
    public function widgetAction(Request $request)
    {
     $user = $this->get('security.context')->getToken()->getUser();
        
        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
      
       $em     = $this->getDoctrine()->getManager();
       $entities = $em->getRepository('CrijaReminderBundle:Event')->findby(array('user' => $user));
        
       return array('entities' => $entities);
    }
   /**
     * Events widget entity.
     *
     * @Route("/events-number-widget", name="number")
     * @Template()
     */
    public function numberAction(Request $request)
    {
     $user = $this->get('security.context')->getToken()->getUser();
        
        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
      
       $em     = $this->getDoctrine()->getManager();
       $entities = $em->getRepository('CrijaReminderBundle:Event')->findby(array('user' => $user));
        
       return array('number' => count($entities));
    }
    
    /**
     * Change completed Event.
     *
     * @Route("/complete-reminder", name="complete-reminder")
     * @Template()
     */
    public function completeReminderAction(Request $request)
    {
       $user            = $this->get('security.context')->getToken()->getUser();
       $eventId         = $request->request->get('event');
       $completedAtGet  = $request->request->get('completedAt');
       $completedAtDate = new \Datetime($completedAtGet);
       
       
        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
      
       $em     = $this->getDoctrine()->getManager();
       $entity = $em->getRepository('CrijaReminderBundle:Event')->findOneby(array('id' => $eventId, 'user' => $user));


       //$date_modify    = str_replace("+", "-", $entity->getPeriodicity());
       //$completedAt    = $completedAtDate->modify($date_modify);
       //$entity->setCompletedAt($completedAt);


        $entity->setCompletedAt($completedAtDate);
       
       $em->persist($entity);
       $em->flush();
       
        return $this->redirect($this->generateUrl('event'));
    }
 /**
     * Delete a Event entity.
     *
     * @Route("/borrar/{id}", name="event_borrar")
     */
    public function borrarAction(Request $request, $id)
    {
            $user   = $this->get('security.context')->getToken()->getUser();
            $em     = $this->getDoctrine()->getManager();
            if (!$user) {
               throw $this->createNotFoundException('Imposible localizar al usuario');
             }
            $entity = $em->getRepository('CrijaReminderBundle:Event')->findOneBy(array('id' => $id, 'user'=> $user));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Event entity.');
            }

            $em->remove($entity);
            $em->flush();
      

        return $this->redirect($this->generateUrl('event'));
    }
    
    
    
}
