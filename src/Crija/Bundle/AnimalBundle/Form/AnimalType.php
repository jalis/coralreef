<?php

namespace Crija\Bundle\AnimalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AnimalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('realName',text,array('required'  => true))
            ->add('englishName')
            ->add('spanishName')
            ->add('size')
            ->add('sex')
            ->add('dificulty', 'choice', array(
                'choices'   => array('Facil' => 'Facil', 'Dificil' => 'Dificil', 'Medio' => 'Medio'),
                'required'  => false,
                'preferred_choices' => array('Facil'),
            ))
            ->add('temperament', 'choice', array(
                'choices'   => array('Pacifico' => 'Pacifico', 'Agresivo' => 'Agresivo', 'Problematico' => 'Problematico'),
                'required'  => false,
                'preferred_choices' => array('Pacifico'),
            ))
            ->add('volume')
            ->add('reefsafe', 'choice', array(
                'choices'   => array('Si' => 'Si', 'Con precaución' => 'Con precaución', 'No' => 'No'),
                'required'  => false,
                'preferred_choices' => array('Si'),
            ))
            ->add('file')
            ->add('description', 'textarea', array('required'  => false,
                'attr' => array('cols' => '148', 'rows' => '3')))
            ->add('behavior', 'textarea', array('required'  => false,
                'attr' => array('cols' => '148', 'rows' => '3')))
            ->add('feeding', 'textarea', array('required'  => false,
                'attr' => array('cols' => '148', 'rows' => '3')))
            
            ->add('animal_category','entity',array(
                'class' => 'CrijaAnimalBundle:AnimalCategory',
                'required' => true,
            ))
        ;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\AnimalBundle\Entity\Animal'
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_animalbundle_animal';
    }
}
