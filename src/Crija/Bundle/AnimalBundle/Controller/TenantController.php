<?php

namespace Crija\Bundle\AnimalBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Crija\Bundle\AnimalBundle\Entity\Tenant;
use Crija\Bundle\AnimalBundle\Form\TenantType;
use Jalis\Bundle\GalleryBundle\Entity\Photo;
use Jalis\Bundle\GalleryBundle\Form\PhotoType;

/**
 * Tenant controller.
 *
 * @Route("/mis-inquilinos")
 */
class TenantController extends Controller
{

    /**
     * Lists all Tenant entities.
     *
     * @Route("/", name="tenant")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $entities = $em->getRepository('CrijaAnimalBundle:Tenant')->findBy(array('user' =>$user));


        foreach($entities as $entity) {



            if($entity->getAnimal()->getAnimalCategory()->getId() == 1)
            {
                $peces[] = $entity;

                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));


            }else if($entity->getAnimal()->getAnimalCategory()->getId() == 2) {
                $corales[] = $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }
            else if($entity->getAnimal()->getAnimalCategory()->getId() == 3) {
                $invertebrados[] = $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }
            else if($entity->getAnimal()->getAnimalCategory()->getId() == 4) {
                $plantas[] = $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }

        }

        $tenants['Peces'] = $peces;
        $tenants['Corales'] = $corales;
        $tenants['Invertebrados'] = $invertebrados;
        $tenants['Plantas y algas'] = $plantas;


        return array(
            'entities' => $entities,
            'all_tenants' => $tenants,
            'gallery' => $gallery

        );
    }

    /**
     * widget
     *
     * @Route("/widget_tenant/{id}", name="widget_tenant")
     * @Method("GET")
     * @Template()
     */
    public function widgetAction($id)
    {
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('JalisUserBundle:User')->findOneBy(array('id' => $id));

        $entities = $em->getRepository('CrijaAnimalBundle:Tenant')->findBy(array('user' =>$user ),array('diedAt' => 'ASC'));


        foreach($entities as $entity) {



            if($entity->getAnimal()->getAnimalCategory()->getId() == 1)
            {
                $peces[] = $entity;

                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));


            }else if($entity->getAnimal()->getAnimalCategory()->getId() == 2) {
                $corales[] = $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }
            else if($entity->getAnimal()->getAnimalCategory()->getId() == 3) {
                $invertebrados[] = $entity;
                $gallery[$entity->getId()] = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));
            }

        }

        $tenants['Peces'] = $peces;
        $tenants['Corales'] = $corales;
        $tenants['Invertebrados'] = $invertebrados;


        return array(
            'entities' => $entities,
            'all_tenants' => $tenants,
            'gallery' => $gallery

        );


    }

    /**
     * Creates a new Tenant entity.
     *
     * @Route("/", name="tenant_create")
     * @Method("POST")
     * @Template("CrijaAnimalBundle:Tenant:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }
        $data =$request->request->get('crija_bundle_animalbundle_tenant');

        $animal_id = $data['animal'];

        if($animal_id == 0) {die("debes seleccionar un tipo de inquilino");}
        $color1 = $request->request->get('color1');
        $color2 = $request->request->get('color2');
        $colors = $color1.";".$color2;


        $em = $this->getDoctrine()->getManager();
        $animal = $em->getRepository('CrijaAnimalBundle:Animal')->findOneBy(array('id' => $animal_id));

        $entity = new Tenant();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $entity->setUser($user);
            $entity->setAnimal($animal);
            $entity->setColors($colors);
            $em->persist($entity);
            $em->flush();

        return $this->redirect($this->generateUrl('tenant_show', array('id' => $entity->getId())));
       

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Tenant entity.
     *
     * @param Tenant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tenant $entity)
    {

        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new TenantType($securityContext), $entity, array(
            'action' => $this->generateUrl('tenant_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tenant entity.
     *
     * @Route("/nuevo", name="tenant_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Tenant();
        $form   = $this->createCreateForm($entity);

        $msg = $request->query->get('msg');

    
        return array(
            'entity' => $entity,
            'msg' => $msg,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Tenant entity.
     *
     * @Route("/{id}", name="tenant_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $owner = false;
        $entity = $em->getRepository('CrijaAnimalBundle:Tenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tenant entity.');
        }


        $photo = new Photo();
        $photo->setObjId($entity->getId());
        $photo->setObjType('Tenant');
        $form = $this->createForm(new PhotoType(), $photo);


        $gallery = $em->getRepository('JalisGalleryBundle:Photo')->findBy(array('obj_id' => $entity->getId(),'obj_type' =>'Tenant'));


        if($entity->getUser() == $user = $this->get('security.context')->getToken()->getUser()) {

            $owner = true;
        }

        return array(
            'entity'      => $entity,
            'animal'      => $entity->getAnimal(),
            'photo'       => $photo,
            'form'        => $form->createView(),
            'gallery'     => $gallery,
            'owner'       => $owner

        );
    }

    /**
     * Displays a form to edit an existing Tenant entity.
     *
     * @Route("/{id}/editar", name="tenant_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }



        $entity = $em->getRepository('CrijaAnimalBundle:Tenant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tenant entity.');
        }

        if($user != $entity->getUser()) {
            throw $this->createNotFoundException('No puedes editar este inquilino.');
        }


        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $colors = explode(";",$entity->getColors());

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'colors' => $colors,
        );
    }

    /**
    * Creates a form to edit a Tenant entity.
    *
    * @param Tenant $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tenant $entity)
    {

        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new TenantType($securityContext), $entity, array(
            'action' => $this->generateUrl('tenant_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Tenant entity.
     *
     * @Route("/{id}", name="tenant_update")
     * @Method("PUT")
     * @Template("CrijaAnimalBundle:Tenant:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CrijaAnimalBundle:Tenant')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tenant entity.');
        }

        $user = $this->get('security.context')->getToken()->getUser();
        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }

        $data =$request->request->get('crija_bundle_animalbundle_tenant');
        $animal_id = $data['animal'];
        if($animal_id == 0) {
            $animal_id = $entity->getAnimal()->getId();
            $data['animal'] = $animal_id;
            $request->request->set('crija_bundle_animalbundle_tenant',  $data);
            $animal = $em->getRepository('CrijaAnimalBundle:Animal')->findOneBy(array('id' => $animal_id));

        } else {
            $animal = $em->getRepository('CrijaAnimalBundle:Animal')->findOneBy(array('id' => $animal_id));
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $entity->setAnimal($animal);
        $color1 = $request->request->get('color1');
        $color2 = $request->request->get('color2');
        $colors = $color1.";".$color2;
        $entity->setColors($colors);

        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('tenant_show', array('id' => $id)));


    }
    /**
     * Deletes a Tenant entity.
     *
     * @Route("/delete/{id}", name="tenant_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CrijaAnimalBundle:Tenant')->find($id);

        
        if($entity->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException('Operacion denegada');
        }

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tenant entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('tenant'));
    }

    /**
     * Creates a form to delete a Tenant entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tenant_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Deletes a Tenant entity.
     *
     * @Route("/die/{id}", name="tenant_die")
     * @Method("GET")
     */
    public function dieAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (!$user) {
            throw $this->createNotFoundException('Imposible localizar al usuario');
        }

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CrijaAnimalBundle:Tenant')->find($id);


        if($entity->getUser()->getId() != $user->getId()) {
            throw $this->createNotFoundException('Operacion denegada');
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tenant entity.');
        }

        $entity->setDiedAt(new \DateTime());
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('tenant'));
    }

}
