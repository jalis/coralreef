<?php

namespace Crija\Bundle\AquariumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityRepository;

class WaterParameterType extends AbstractType
{
    private $securityContext;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    
      $user = $this->securityContext->getToken()->getUser();
      $user_id = (int)$user->getId();
     
      $builder->add('salinity','number', array('precision' => 2,'required' => false))
            ->add('calcium','number', array('precision' => 2,'required' => false))
            ->add('alkalinity','number', array('precision' => 2,'required' => false))
            ->add('temperature','number', array('precision' => 2,'required' => false))
            ->add('ph','number', array('precision' => 2,'required' => false))
            ->add('magnesium','number', array('precision' => 2,'required' => false))
            ->add('phosphate','number', array('precision' => 2,'required' => false))
            ->add('ammonia','number', array('precision' => 2,'required' => false))
            ->add('silica','number', array('precision' => 2,'required' => false))
            ->add('iodine','number', array('precision' => 2,'required' => false))
            ->add('nitrate','number', array('precision' => 2,'required' => false))
            ->add('nitrite','number', array('precision' => 2,'required' => false))
            ->add('strontium','number', array('precision' => 2,'required' => false))
            ->add('boron','number', array('precision' => 2,'required' => false))
            ->add('iron','number', array('precision' => 2,'required' => false))
          ->add('potassium','number', array('precision' => 2,'required' => false))
            ->add('description')
            ->add('createdAt', null, array('widget' => 'single_text','data'  => date_create()))
            ->add('updatedAt', null, array('widget' => 'single_text','data'  => date_create()))
            //->add('aquarium',null,array('empty_value' => false))
            ->add('aquarium', 'entity',
                  array(
                      'class'         => 'CrijaAquariumBundle:Aquarium',
                      'query_builder' => function(EntityRepository $er) use ($user_id) {
                          return $er->createQueryBuilder('i')->andWhere('i.user = :user_id')->setParameter('user_id', $user_id);
                           }
                        )
               );
        
        
       
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\AquariumBundle\Entity\WaterParameter'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_aquariumbundle_waterparameter';
    }
}
