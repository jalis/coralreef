<?php

namespace Crija\Bundle\AquariumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityRepository;


class DeviceType extends AbstractType
{
    private $securityContext;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user = $this->securityContext->getToken()->getUser();
        $user_id = (int)$user->getId();

        $builder
            ->add('marca')
            ->add('modelo')
            ->add('watios')
            ->add('precio')
            ->add('descripcion')
            //->add('tienda')
            ->add('horas')
            ->add('unidades')
            ->add('tipo', 'choice', array(
                        'choices' => array(
                        'bomba circulacion' => 'bomba circulacion',
                        'bomba subida' => 'bomba subída',
                        'pantalla' => 'pantalla',
                        'rellenador' => 'rellenador',
                        'uv' => 'lampara UV',
                        'skimmer' => 'skimmer',
                        'filtro' => 'filtro',
                        'osmosis' => 'osmosis',
                        'peristaltica' => 'peristaltica',
                        'enfriador' => 'enfriador',
                        'calentador' => 'calentador',
                        'ventiladore' => 'ventilador',
                        'controladora' => 'controladora',
                        'reactor' => 'reactor',
                        'peristaltica' => 'peristaltica',
                        'sensor' => 'sensor',
                        'otros' =>'otros')
                )
            )
            ->add('estado')
          //  ->add('createdAt', null, array('widget' => 'single_text','data'  => date_create()))
            ->add('aquarium', 'entity',
                array(
                    'class'         => 'CrijaAquariumBundle:Aquarium',
                    'query_builder' => function(EntityRepository $er) use ($user_id) {
                        return $er->createQueryBuilder('i')->andWhere('i.user = :user_id')->setParameter('user_id', $user_id);
                    }
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Crija\Bundle\AquariumBundle\Entity\Device'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_aquariumbundle_device';
    }
}
