<?php

namespace Crija\Bundle\AquariumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AquariumType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('litres')
            ->add('type', 'choice', array(
			    	'choices' => array('salada' => 'salada', 'dulce' => 'dulce'),
			    	)
			     )
            ->add('description')
            ->add('name')
            ->add('createdAt', null, array('widget' => 'single_text','data'  => date_create()))
            ->add('updatedAt')
            //->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '\Crija\Bundle\AquariumBundle\Entity\Aquarium'
        ));
        
        
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crija_bundle_aquariumbundle_aquarium';
    }
    
   
    
}
