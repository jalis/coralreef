<?php

namespace Crija\Bundle\AquariumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Aquarium
 *
 * @ORM\Table(name="aquarium")
 * @ORM\Entity(repositoryClass="Crija\Bundle\AquariumBundle\Entity\AquariumRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Aquarium
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="litres", type="integer", nullable=true)
     */
    private $litres;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /** 
     * created Time/Date 
     * 
     * @var \DateTime 
     * 
     * @ORM\Column(name="created_at", type="datetime", nullable=false) 
     */  
    protected $createdAt;  
  
    /** 
     * updated Time/Date 
     * 
     * @var \DateTime 
     * 
     * @ORM\Column(name="updated_at", type="datetime", nullable=true) 
     */  
    protected $updatedAt; 
    
    /**
     * @Orm\ManyToOne(targetEntity="Jalis\Bundle\UserBundle\Entity\User", inversedBy="aquariums")
     */
    private $user;
    
    /**
     * @Orm\OneToMany(targetEntity="Crija\Bundle\AquariumBundle\Entity\WaterParameter", mappedBy="aquarium",cascade={"remove"})
     * @Orm\OrderBy({"createdAt" = "DESC"})
     */
    private $waterParameters;

    /**
     * @Orm\OneToMany(targetEntity="Crija\Bundle\AnimalBundle\Entity\Tenant", mappedBy="aquarium",cascade={"remove"})
     * @Orm\OrderBy({"createdAt" = "ASC"})
     */
    private $tenants;

    /**
     * @Orm\OneToMany(targetEntity="Crija\Bundle\AquariumBundle\Entity\Device", mappedBy="aquarium",cascade={"remove"})
     * @Orm\OrderBy({"createdAt" = "ASC"})
     */
    private $devices;
  
  
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->waterParameters = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tenants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->devices = new \Doctrine\Common\Collections\ArrayCollection();
    }

	/**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString() {

	return $this->name;
    }

    /**
     * Set litres
     *
     * @param integer $litres
     * @return Aquarium
     */
    public function setLitres($litres)
    {
        $this->litres = $litres;

        return $this;
    }

    /**
     * Get litres
     *
     * @return integer 
     */
    public function getLitres()
    {
        return $this->litres;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Aquarium
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Aquarium
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return string
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Aquarium
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Aquarium
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \Jalis\Bundle\UserBundle\Entity\User $user
     * @return Aquarium
     */
    public function setUser(\Jalis\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jalis\Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add waterParameters
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\WaterParameter $waterParameters
     * @return Aquarium
     */
    public function addWaterParameter(\Crija\Bundle\AquariumBundle\Entity\WaterParameter $waterParameters)
    {
        $this->waterParameters[] = $waterParameters;

        return $this;
    }

    /**
     * Remove waterParameters
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\WaterParameter $waterParameters
     */
    public function removeWaterParameter(\Crija\Bundle\AquariumBundle\Entity\WaterParameter $waterParameters)
    {
        $this->waterParameters->removeElement($waterParameters);
    }

    /**
     * Get waterParameters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWaterParameters()
    {
        return $this->waterParameters;
    }


    /**
     * Add Devices
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Devices $devices
     * @return Aquarium
     */
    public function addDevice(\Crija\Bundle\AquariumBundle\Entity\Device $devices)
    {
        $this->devices[] = $devices;

        return $this;
    }

    /**
     * Remove devices
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\WaterParameter $waterParameters
     */
    public function removeDevice(\Crija\Bundle\AquariumBundle\Entity\Device $devices)
    {
        $this->devices->removeElement($devices);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevice()
    {
        return $this->devices;
    }
}
