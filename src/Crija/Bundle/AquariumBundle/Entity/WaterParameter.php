<?php

namespace Crija\Bundle\AquariumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parameters
 *
 * @ORM\Table(name="water_parameter")
 * @ORM\Entity(repositoryClass="Crija\Bundle\AquariumBundle\Entity\WaterParameterRepository")
 * @ORM\HasLifecycleCallbacks
 */
class WaterParameter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="salinity", type="decimal", nullable=true)
     */
    private $salinity;
    /**
     * @var integer
     *
     * @ORM\Column(name="calcium", type="decimal", nullable=true)
     */
    private $calcium;

    /**
     * @var integer
     *
     * @ORM\Column(name="alkalinity", type="decimal", nullable=true)
     */
    private $alkalinity;

    /**
     * @var integer
     *
     * @ORM\Column(name="temperature", type="decimal", nullable=true)
     */
    private $temperature;

    /**
     * @var integer
     *
     * @ORM\Column(name="ph", type="decimal", nullable=true)
     */
    private $ph;

    /**
     * @var integer
     *
     * @ORM\Column(name="magnesium", type="decimal", nullable=true)
     */
    private $magnesium;

    /**
     * @var integer
     *
     * @ORM\Column(name="phosphate", type="decimal", nullable=true)
     */
    private $phosphate;

    /**
     * @var integer
     *
     * @ORM\Column(name="ammonia", type="decimal", nullable=true)
     */
    private $ammonia;

    /**
     * @var integer
     *
     * @ORM\Column(name="silica", type="decimal", nullable=true)
     */
    private $silica;

    /**
     * @var integer
     *
     * @ORM\Column(name="iodine", type="decimal", nullable=true)
     */
    private $iodine;

    /**
     * @var integer
     *
     * @ORM\Column(name="nitrate", type="decimal", nullable=true)
     */
    private $nitrate;

    /**
     * @var integer
     *
     * @ORM\Column(name="nitrite", type="decimal", nullable=true)
     */
    private $nitrite;

    /**
     * @var integer
     *
     * @ORM\Column(name="strontium", type="decimal", nullable=true)
     */
    private $strontium;

    /**
     * @var integer
     *
     * @ORM\Column(name="boron", type="decimal", nullable=true)
     */
    private $boron;

    /**
     * @var integer
     *
     * @ORM\Column(name="iron", type="decimal", nullable=true)
     */
    private $iron;

    /**
     * @var integer
     *
     * @ORM\Column(name="potassium", type="decimal", nullable=true)
     */
    private $potassium;



    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /** 
     * created Time/Date 
     * 
     * @var \DateTime 
     * 
     * @ORM\Column(name="created_at", type="datetime", nullable=false) 
     */  
    protected $createdAt;  
  
    /** 
     * updated Time/Date 
     * 
     * @var \DateTime 
     * 
     * @ORM\Column(name="updated_at", type="datetime", nullable=false) 
     */  
    protected $updatedAt; 
     /**
     * @Orm\ManyToOne(targetEntity="Aquarium", inversedBy="water_parameters")
     */
    private $aquarium;
 
  
    /** 
     * Set createdAt
     */  
    public function setCreatedAt($createdAt)
    {  
        $this->createdAt = $createdAt;

       // $this->updatedAt = new \DateTime();
        return $this;

    }  
  
    /** 
     * Get createdAt 
     * 
     * @return \DateTime 
     */  
    public function getCreatedAt()  
    {  
        return $this->createdAt;  
    }  
  
    /** 
     * Set updatedAt 
     * 
     * @ORM\PreUpdate 
     */  
    public function setUpdatedAt()  
    {  
        $this->updatedAt = new \DateTime();  
    }  
  
    /** 
     * Get updatedAt 
     * 
     * @return \DateTime 
     */  
    public function getUpdatedAt()  
    {  
        return $this->updatedAt;  
    }  

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Set salinity
     *
     * @param integer $salinity
     * @return WaterParameter
     */
    public function setSalinity($salinity)
    {
        $this->salinity = $salinity;

        return $this;
    }

    /**
     * Get salinity
     *
     * @return integer 
     */
    public function getSalinity()
    {
        return $this->salinity;
    }

    /**
     * Set calcium
     *
     * @param integer $calcium
     * @return WaterParameter
     */
    public function setCalcium($calcium)
    {
        $this->calcium = $calcium;

        return $this;
    }

    /**
     * Get calcium
     *
     * @return integer 
     */
    public function getCalcium()
    {
        return $this->calcium;
    }

    /**
     * Set alkalinity
     *
     * @param integer $alkalinity
     * @return WaterParameter
     */
    public function setAlkalinity($alkalinity)
    {
        $this->alkalinity = $alkalinity;

        return $this;
    }

    /**
     * Get alkalinity
     *
     * @return integer 
     */
    public function getAlkalinity()
    {
        return $this->alkalinity;
    }

    /**
     * Set temperature
     *
     * @param integer $temperature
     * @return WaterParameter
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return integer 
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set ph
     *
     * @param integer $ph
     * @return WaterParameter
     */
    public function setPh($ph)
    {
        $this->ph = $ph;

        return $this;
    }

    /**
     * Get ph
     *
     * @return integer 
     */
    public function getPh()
    {
        return $this->ph;
    }

    /**
     * Set magnesium
     *
     * @param integer $magnesium
     * @return WaterParameter
     */
    public function setMagnesium($magnesium)
    {
        $this->magnesium = $magnesium;

        return $this;
    }

    /**
     * Get magnesium
     *
     * @return integer 
     */
    public function getMagnesium()
    {
        return $this->magnesium;
    }

    /**
     * Set phosphate
     *
     * @param integer $phosphate
     * @return WaterParameter
     */
    public function setPhosphate($phosphate)
    {
        $this->phosphate = $phosphate;

        return $this;
    }

    /**
     * Get phosphate
     *
     * @return integer 
     */
    public function getPhosphate()
    {
        return $this->phosphate;
    }

    /**
     * Set ammonia
     *
     * @param integer $ammonia
     * @return WaterParameter
     */
    public function setAmmonia($ammonia)
    {
        $this->ammonia = $ammonia;

        return $this;
    }

    /**
     * Get ammonia
     *
     * @return integer 
     */
    public function getAmmonia()
    {
        return $this->ammonia;
    }

    /**
     * Set silica
     *
     * @param integer $silica
     * @return WaterParameter
     */
    public function setSilica($silica)
    {
        $this->silica = $silica;

        return $this;
    }

    /**
     * Get silica
     *
     * @return integer 
     */
    public function getSilica()
    {
        return $this->silica;
    }

    /**
     * Set iodine
     *
     * @param integer $iodine
     * @return WaterParameter
     */
    public function setIodine($iodine)
    {
        $this->iodine = $iodine;

        return $this;
    }

    /**
     * Get iodine
     *
     * @return integer 
     */
    public function getIodine()
    {
        return $this->iodine;
    }

    /**
     * Set nitrate
     *
     * @param integer $nitrate
     * @return WaterParameter
     */
    public function setNitrate($nitrate)
    {
        $this->nitrate = $nitrate;

        return $this;
    }

    /**
     * Get nitrate
     *
     * @return integer 
     */
    public function getNitrate()
    {
        return $this->nitrate;
    }

    /**
     * Set nitrite
     *
     * @param integer $nitrite
     * @return WaterParameter
     */
    public function setNitrite($nitrite)
    {
        $this->nitrite = $nitrite;

        return $this;
    }

    /**
     * Get nitrite
     *
     * @return integer 
     */
    public function getNitrite()
    {
        return $this->nitrite;
    }

    /**
     * Set strontium
     *
     * @param integer $strontium
     * @return WaterParameter
     */
    public function setStrontium($strontium)
    {
        $this->strontium = $strontium;

        return $this;
    }

    /**
     * Get strontium
     *
     * @return integer 
     */
    public function getStrontium()
    {
        return $this->strontium;
    }

    /**
     * Set boron
     *
     * @param integer $boron
     * @return WaterParameter
     */
    public function setBoron($boron)
    {
        $this->boron = $boron;

        return $this;
    }

    /**
     * Get boron
     *
     * @return integer 
     */
    public function getBoron()
    {
        return $this->boron;
    }

    /**
     * Set iron
     *
     * @param integer $iron
     * @return WaterParameter
     */
    public function setIron($iron)
    {
        $this->iron = $iron;

        return $this;
    }

    /**
     * Get iron
     *
     * @return integer 
     */
    public function getIron()
    {
        return $this->iron;
    }


    /**
     * @return int
     */
    public function getPotassium()
    {
        return $this->potassium;
    }

    /**
     * @param int $potassium
     */
    public function setPotassium($potassium)
    {
        $this->potassium = $potassium;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return WaterParameter
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set aquarium
     *
     * @param \Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium
     * @return WaterParameter
     */
    public function setAquarium(\Crija\Bundle\AquariumBundle\Entity\Aquarium $aquarium = null)
    {
        $this->aquarium = $aquarium;

        return $this;
    }

    /**
     * Get aquarium
     *
     * @return \Crija\Bundle\AquariumBundle\Entity\Aquarium 
     */
    public function getAquarium()
    {
        return $this->aquarium;
    }
}
